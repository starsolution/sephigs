'use strict';

var should = require('should'),
	request = require('supertest'),
	app = require('../../server'),
	mongoose = require('mongoose'),
	User = mongoose.model('User'),
	Wig = mongoose.model('Wig'),
	agent = request.agent(app);

/**
 * Globals
 */
var credentials, user, wig;

/**
 * Wig routes tests
 */
describe('Wig CRUD tests', function() {
	beforeEach(function(done) {
		// Create user credentials
		credentials = {
			username: 'username',
			password: 'password'
		};

		// Create a new user
		user = new User({
			firstName: 'Full',
			lastName: 'Name',
			displayName: 'Full Name',
			email: 'test@test.com',
			username: credentials.username,
			password: credentials.password,
			provider: 'local'
		});

		// Save a user to the test db and create new Wig
		user.save(function() {
			wig = {
				name: 'Wig Name'
			};

			done();
		});
	});

	it('should be able to save Wig instance if logged in', function(done) {
		agent.post('/auth/signin')
			.send(credentials)
			.expect(200)
			.end(function(signinErr, signinRes) {
				// Handle signin error
				if (signinErr) done(signinErr);

				// Get the userId
				var userId = user.id;

				// Save a new Wig
				agent.post('/wigs')
					.send(wig)
					.expect(200)
					.end(function(wigSaveErr, wigSaveRes) {
						// Handle Wig save error
						if (wigSaveErr) done(wigSaveErr);

						// Get a list of Wigs
						agent.get('/wigs')
							.end(function(wigsGetErr, wigsGetRes) {
								// Handle Wig save error
								if (wigsGetErr) done(wigsGetErr);

								// Get Wigs list
								var wigs = wigsGetRes.body;

								// Set assertions
								(wigs[0].user._id).should.equal(userId);
								(wigs[0].name).should.match('Wig Name');

								// Call the assertion callback
								done();
							});
					});
			});
	});

	it('should not be able to save Wig instance if not logged in', function(done) {
		agent.post('/wigs')
			.send(wig)
			.expect(401)
			.end(function(wigSaveErr, wigSaveRes) {
				// Call the assertion callback
				done(wigSaveErr);
			});
	});

	it('should not be able to save Wig instance if no name is provided', function(done) {
		// Invalidate name field
		wig.name = '';

		agent.post('/auth/signin')
			.send(credentials)
			.expect(200)
			.end(function(signinErr, signinRes) {
				// Handle signin error
				if (signinErr) done(signinErr);

				// Get the userId
				var userId = user.id;

				// Save a new Wig
				agent.post('/wigs')
					.send(wig)
					.expect(400)
					.end(function(wigSaveErr, wigSaveRes) {
						// Set message assertion
						(wigSaveRes.body.message).should.match('Please fill Wig name');
						
						// Handle Wig save error
						done(wigSaveErr);
					});
			});
	});

	it('should be able to update Wig instance if signed in', function(done) {
		agent.post('/auth/signin')
			.send(credentials)
			.expect(200)
			.end(function(signinErr, signinRes) {
				// Handle signin error
				if (signinErr) done(signinErr);

				// Get the userId
				var userId = user.id;

				// Save a new Wig
				agent.post('/wigs')
					.send(wig)
					.expect(200)
					.end(function(wigSaveErr, wigSaveRes) {
						// Handle Wig save error
						if (wigSaveErr) done(wigSaveErr);

						// Update Wig name
						wig.name = 'WHY YOU GOTTA BE SO MEAN?';

						// Update existing Wig
						agent.put('/wigs/' + wigSaveRes.body._id)
							.send(wig)
							.expect(200)
							.end(function(wigUpdateErr, wigUpdateRes) {
								// Handle Wig update error
								if (wigUpdateErr) done(wigUpdateErr);

								// Set assertions
								(wigUpdateRes.body._id).should.equal(wigSaveRes.body._id);
								(wigUpdateRes.body.name).should.match('WHY YOU GOTTA BE SO MEAN?');

								// Call the assertion callback
								done();
							});
					});
			});
	});

	it('should be able to get a list of Wigs if not signed in', function(done) {
		// Create new Wig model instance
		var wigObj = new Wig(wig);

		// Save the Wig
		wigObj.save(function() {
			// Request Wigs
			request(app).get('/wigs')
				.end(function(req, res) {
					// Set assertion
					res.body.should.be.an.Array.with.lengthOf(1);

					// Call the assertion callback
					done();
				});

		});
	});


	it('should be able to get a single Wig if not signed in', function(done) {
		// Create new Wig model instance
		var wigObj = new Wig(wig);

		// Save the Wig
		wigObj.save(function() {
			request(app).get('/wigs/' + wigObj._id)
				.end(function(req, res) {
					// Set assertion
					res.body.should.be.an.Object.with.property('name', wig.name);

					// Call the assertion callback
					done();
				});
		});
	});

	it('should be able to delete Wig instance if signed in', function(done) {
		agent.post('/auth/signin')
			.send(credentials)
			.expect(200)
			.end(function(signinErr, signinRes) {
				// Handle signin error
				if (signinErr) done(signinErr);

				// Get the userId
				var userId = user.id;

				// Save a new Wig
				agent.post('/wigs')
					.send(wig)
					.expect(200)
					.end(function(wigSaveErr, wigSaveRes) {
						// Handle Wig save error
						if (wigSaveErr) done(wigSaveErr);

						// Delete existing Wig
						agent.delete('/wigs/' + wigSaveRes.body._id)
							.send(wig)
							.expect(200)
							.end(function(wigDeleteErr, wigDeleteRes) {
								// Handle Wig error error
								if (wigDeleteErr) done(wigDeleteErr);

								// Set assertions
								(wigDeleteRes.body._id).should.equal(wigSaveRes.body._id);

								// Call the assertion callback
								done();
							});
					});
			});
	});

	it('should not be able to delete Wig instance if not signed in', function(done) {
		// Set Wig user 
		wig.user = user;

		// Create new Wig model instance
		var wigObj = new Wig(wig);

		// Save the Wig
		wigObj.save(function() {
			// Try deleting Wig
			request(app).delete('/wigs/' + wigObj._id)
			.expect(401)
			.end(function(wigDeleteErr, wigDeleteRes) {
				// Set message assertion
				(wigDeleteRes.body.message).should.match('User is not logged in');

				// Handle Wig error error
				done(wigDeleteErr);
			});

		});
	});

	afterEach(function(done) {
		User.remove().exec();
		Wig.remove().exec();
		done();
	});
});