'use strict';

module.exports = function(app) {
	var users = require('../../app/controllers/users.server.controller');
	var wigs = require('../../app/controllers/wigs.server.controller');
	var multer  = require('multer');
	var express = require('express');
	//var fs = require('fs');

	// Wigs Routes
	app.use(multer({ dest: './public/uploads/'}));

	//app.route('/publicfiles').get(wigs.publicfiles);
	
	// respond with "hello world" when a GET request is made to the homepage
	app.get('/image/*', function(req, res) {
		var filename = req.originalUrl.split('/');
  		res.sendfile('public/uploads/'+filename[filename.length-1]);
	});
	
	//app.use('/publicuploads', express.static('../../public/uploads'));
	//app.use('/static', express.static(__dirname + '/public/uploads'));
	
			
	app.route('/wigs')
		.get(wigs.list)
		.post(users.requiresLogin, wigs.create);
	
	app.route('/wigs/:wigId')
		.get(wigs.read)
		.put(users.requiresLogin, wigs.hasAuthorization, wigs.update)
		.delete(users.requiresLogin, wigs.hasAuthorization, wigs.delete);

	// Finish by binding the Wig middleware
	app.param('wigId', wigs.wigByID);
	
	app.route('/wigimage/:wigId')
		.post(users.requiresLogin, wigs.addimage);
};
