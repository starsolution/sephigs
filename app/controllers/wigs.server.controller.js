'use strict';



/**
 * Module dependencies.
 */
var mongoose = require('mongoose'),
	errorHandler = require('./errors.server.controller'),
	Wig = mongoose.model('Wig'),
	_ = require('lodash');

/**
 * Create a Wig
 */
exports.create = function(req, res) {
	var wig = new Wig(req.body);
	wig.user = req.user;
	
	//var fs = require('fs');
	
	//save image in DB
	//wig.image = req.files.file.path;
	//if (typeof(req.files.file) !== 'undefined') {	
		/*	
		fs.readFile(req.files.file.path, function (err,original_data) {
		 if (err) {
		      return res.status(400).send({
		            message: errorHandler.getErrorMessage(err)
		        });
		  } 
		  // save image in db as base64 encoded - this limits the image size
		  // to there should be size checks here and in client
		  var base64Image = original_data.toString('base64');
		  */
		  /*
		  fs.unlink(file.path, function (err) {
		      if (err)
		      { 
		          console.log('failed to delete ' + file.path);
		      }
		      else{
		        console.log('successfully deleted ' + file.path);
		      }
		  });
		  */
		 /* 
		  wig.image = base64Image;
		});
		*/
		//wig.image = req.files.file.name;
	//}

	wig.save(function(err) {
		if (err) {
			return res.status(400).send({
				message: errorHandler.getErrorMessage(err)
			});
		} else {
			res.jsonp(wig);
		}
	});	
};

/**
 * add image
 */
exports.addimage = function(req, res) {
	var wig = req.wig ;

	if (typeof(req.files.file) !== 'undefined') {	
		wig.image = req.files.file.name;
	}

	wig.save(function(err) {
		if (err) {
			return res.status(400).send({
				message: errorHandler.getErrorMessage(err)
			});
		} else {
			res.jsonp(wig);
		}
	});	
};

/**
 * Show the current Wig
 */
exports.read = function(req, res) {
	res.jsonp(req.wig);
};

/**
 * Update a Wig
 */
exports.update = function(req, res) {
	var wig = req.wig ;

	wig = _.extend(wig , req.body);

	wig.save(function(err) {
		if (err) {
			return res.status(400).send({
				message: errorHandler.getErrorMessage(err)
			});
		} else {
			res.jsonp(wig);
		}
	});
};

/**
 * Delete an Wig
 */
exports.delete = function(req, res) {
	var wig = req.wig ;

	wig.remove(function(err) {
		if (err) {
			return res.status(400).send({
				message: errorHandler.getErrorMessage(err)
			});
		} else {
			res.jsonp(wig);
		}
	});
};

/**
 * List of Wigs
 */
exports.list = function(req, res) { 
	Wig.find().sort('-created').populate('user', 'displayName').exec(function(err, wigs) {
		if (err) {
			return res.status(400).send({
				message: errorHandler.getErrorMessage(err)
			});
		} else {
			res.jsonp(wigs);
		}
	});
};

/**
 * Wig middleware
 */
exports.wigByID = function(req, res, next, id) { 
	Wig.findById(id).populate('user', 'displayName').exec(function(err, wig) {
		if (err) return next(err);
		if (! wig) return next(new Error('Failed to load Wig ' + id));
		req.wig = wig ;
		next();
	});
};

/**
 * Wig authorization middleware
 */
exports.hasAuthorization = function(req, res, next) {
	if (req.wig.user.id !== req.user.id) {
		return res.status(403).send('User is not authorized');
	}
	next();
};
