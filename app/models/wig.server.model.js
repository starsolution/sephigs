'use strict';

/**
 * Module dependencies.
 */
var mongoose = require('mongoose'),
	Schema = mongoose.Schema;

/**
 * Wig Schema
 */
var WigSchema = new Schema({
	name: {
		type: String,
		default: '',
		required: 'Please fill Wig name',
		trim: true
	},
	description: {
		type: String,
		default: '',
		trim: true
	},
	image: {
		type: String,
		default: ''
	},
	created: {
		type: Date,
		default: Date.now
	},
	user: {
		type: Schema.ObjectId,
		ref: 'User'
	}
});

mongoose.model('Wig', WigSchema);