'use strict';

//Wigs service used to communicate Wigs REST endpoints
angular.module('wigs').factory('Wigs', ['$resource',
	function($resource) {
		return $resource('wigs/:wigId', { wigId: '@_id'
		}, {
			update: {
				method: 'PUT'
			}
		});
	}
]);