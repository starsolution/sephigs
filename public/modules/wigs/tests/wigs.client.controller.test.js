'use strict';

(function() {
	// Wigs Controller Spec
	describe('Wigs Controller Tests', function() {
		// Initialize global variables
		var WigsController,
		scope,
		$httpBackend,
		$stateParams,
		$location;

		// The $resource service augments the response object with methods for updating and deleting the resource.
		// If we were to use the standard toEqual matcher, our tests would fail because the test values would not match
		// the responses exactly. To solve the problem, we define a new toEqualData Jasmine matcher.
		// When the toEqualData matcher compares two objects, it takes only object properties into
		// account and ignores methods.
		beforeEach(function() {
			jasmine.addMatchers({
				toEqualData: function(util, customEqualityTesters) {
					return {
						compare: function(actual, expected) {
							return {
								pass: angular.equals(actual, expected)
							};
						}
					};
				}
			});
		});

		// Then we can start by loading the main application module
		beforeEach(module(ApplicationConfiguration.applicationModuleName));

		// The injector ignores leading and trailing underscores here (i.e. _$httpBackend_).
		// This allows us to inject a service but then attach it to a variable
		// with the same name as the service.
		beforeEach(inject(function($controller, $rootScope, _$location_, _$stateParams_, _$httpBackend_) {
			// Set a new global scope
			scope = $rootScope.$new();

			// Point global variables to injected services
			$stateParams = _$stateParams_;
			$httpBackend = _$httpBackend_;
			$location = _$location_;

			// Initialize the Wigs controller.
			WigsController = $controller('WigsController', {
				$scope: scope
			});
		}));

		it('$scope.find() should create an array with at least one Wig object fetched from XHR', inject(function(Wigs) {
			// Create sample Wig using the Wigs service
			var sampleWig = new Wigs({
				name: 'New Wig'
			});

			// Create a sample Wigs array that includes the new Wig
			var sampleWigs = [sampleWig];

			// Set GET response
			$httpBackend.expectGET('wigs').respond(sampleWigs);

			// Run controller functionality
			scope.find();
			$httpBackend.flush();

			// Test scope value
			expect(scope.wigs).toEqualData(sampleWigs);
		}));

		it('$scope.findOne() should create an array with one Wig object fetched from XHR using a wigId URL parameter', inject(function(Wigs) {
			// Define a sample Wig object
			var sampleWig = new Wigs({
				name: 'New Wig'
			});

			// Set the URL parameter
			$stateParams.wigId = '525a8422f6d0f87f0e407a33';

			// Set GET response
			$httpBackend.expectGET(/wigs\/([0-9a-fA-F]{24})$/).respond(sampleWig);

			// Run controller functionality
			scope.findOne();
			$httpBackend.flush();

			// Test scope value
			expect(scope.wig).toEqualData(sampleWig);
		}));

		it('$scope.create() with valid form data should send a POST request with the form input values and then locate to new object URL', inject(function(Wigs) {
			// Create a sample Wig object
			var sampleWigPostData = new Wigs({
				name: 'New Wig'
			});

			// Create a sample Wig response
			var sampleWigResponse = new Wigs({
				_id: '525cf20451979dea2c000001',
				name: 'New Wig'
			});

			// Fixture mock form input values
			scope.name = 'New Wig';

			// Set POST response
			$httpBackend.expectPOST('wigs', sampleWigPostData).respond(sampleWigResponse);

			// Run controller functionality
			scope.create();
			$httpBackend.flush();

			// Test form inputs are reset
			expect(scope.name).toEqual('');

			// Test URL redirection after the Wig was created
			expect($location.path()).toBe('/wigs/' + sampleWigResponse._id);
		}));

		it('$scope.update() should update a valid Wig', inject(function(Wigs) {
			// Define a sample Wig put data
			var sampleWigPutData = new Wigs({
				_id: '525cf20451979dea2c000001',
				name: 'New Wig'
			});

			// Mock Wig in scope
			scope.wig = sampleWigPutData;

			// Set PUT response
			$httpBackend.expectPUT(/wigs\/([0-9a-fA-F]{24})$/).respond();

			// Run controller functionality
			scope.update();
			$httpBackend.flush();

			// Test URL location to new object
			expect($location.path()).toBe('/wigs/' + sampleWigPutData._id);
		}));

		it('$scope.remove() should send a DELETE request with a valid wigId and remove the Wig from the scope', inject(function(Wigs) {
			// Create new Wig object
			var sampleWig = new Wigs({
				_id: '525a8422f6d0f87f0e407a33'
			});

			// Create new Wigs array and include the Wig
			scope.wigs = [sampleWig];

			// Set expected DELETE response
			$httpBackend.expectDELETE(/wigs\/([0-9a-fA-F]{24})$/).respond(204);

			// Run controller functionality
			scope.remove(sampleWig);
			$httpBackend.flush();

			// Test array after successful delete
			expect(scope.wigs.length).toBe(0);
		}));
	});
}());