'use strict';

//angular.module('wigs', ['ngFileUpload']);
 
// Wigs controller
angular.module('wigs')
.controller('WigsController', ['$scope', '$stateParams', '$location','$http','$rootScope', 'Authentication', 'Wigs',
	function($scope, $stateParams, $location, $http, $rootScope, Authentication, Wigs) {
		
		$scope.authentication = Authentication;

		// Create new Wig    
    	$scope.create = function() {
			var wig = new Wigs({
				name: this.name,
				description: this.description
				//image: $scope.files[0].name
			});
			wig.$save(function(response) {
	       		$scope.saveImage($scope.files[0],response._id);
			}, function(errorResponse) {
				$scope.error = errorResponse.data.message;
			});
		};
		
		// Remove existing Wig
		$scope.remove = function(wig) {
			if ( wig ) { 
				wig.$remove();

				for (var i in $scope.wigs) {
					if ($scope.wigs [i] === wig) {
						$scope.wigs.splice(i, 1);
					}
				}
			} else {
				$scope.wig.$remove(function() {
					$location.path('wigs');
				});
			}
		};

		// Update existing Wig
		$scope.update = function() {
			var wig = $scope.wig;           
			wig.$update(function() {
	       		$scope.saveImage($scope.files[0],wig._id);			
			}, function(errorResponse) {
				$scope.error = errorResponse.data.message;
			});
		};

		// Find a list of Wigs
		$scope.find = function() {
			$scope.wigs = Wigs.query();
		};

		// Find existing Wig
		$scope.findOne = function() {
			$scope.wig = Wigs.get({ 
				wigId: $stateParams.wigId
			});
		};
		
		$scope.saveImage = function(image, wig_id) {
		    var fd = new FormData();
	        fd.append('file', image);
	        $http.post('wigimage/' + wig_id, fd, {
	        	transformRequest: angular.identity,
	         	headers: {'Content-Type': undefined}
	     	})
	     	.success(function(){
	         	console.log('success add new image');
	         	// Clear form fields
	         	$location.path('wigs/' + wig_id);
				$scope.name = '';
				$scope.description = '';
	     	})
	     	.error(function(e){
	         	console.log('error add new image', e);
	         	$scope.error = e.data.message;
	     	});
    	};
	}
]);