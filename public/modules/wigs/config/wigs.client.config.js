'use strict';

// Configuring the Articles module
angular.module('wigs').run(['Menus',
	function(Menus) {
		// Set top bar menu items
		Menus.addMenuItem('topbar', 'Wigs', 'wigs', 'dropdown', '/wigs(/create)?');
		Menus.addSubMenuItem('topbar', 'wigs', 'List Wigs', 'wigs');
		Menus.addSubMenuItem('topbar', 'wigs', 'New Wig', 'wigs/create');
	}
]);

//angular.module('wigs', ['flow']);