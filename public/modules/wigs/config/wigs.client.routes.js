'use strict';

//Setting up route
angular.module('wigs').config(['$stateProvider',
	function($stateProvider) {
		// Wigs state routing
		$stateProvider.
		state('listWigs', {
			url: '/wigs',
			templateUrl: 'modules/wigs/views/list-wigs.client.view.html'
		}).
		state('createWig', {
			url: '/wigs/create',
			templateUrl: 'modules/wigs/views/create-wig.client.view.html'
		}).
		state('viewWig', {
			url: '/wigs/:wigId',
			templateUrl: 'modules/wigs/views/view-wig.client.view.html'
		}).
		state('editWig', {
			url: '/wigs/:wigId/edit',
			templateUrl: 'modules/wigs/views/edit-wig.client.view.html'
		});
	}
]);